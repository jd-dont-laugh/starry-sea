package com.springboot.starrysea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author junwu.yang
 */
@SpringBootApplication
public class StarrySeaApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarrySeaApplication.class, args);
    }

}

package com.springboot.starrysea.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Accessors(chain = true)
public class SysRole implements Serializable {
    private Integer id;
    private Integer levelId;
    private String admin;
    private Date createTime;
    private Date updateTime;
}

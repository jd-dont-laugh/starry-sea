package com.springboot.starrysea.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("All")
@Accessors(chain = true)
@ToString
public class Tree {


        private String id;//主键
        private String parentCode;//父级编码
        private String treeName;//树结构的名字

        private String isParent;//是否是父极
        private String treeSort;//排序
        private String treeImg;//树结构的图片
}

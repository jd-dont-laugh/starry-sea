package com.springboot.starrysea.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Accessors(chain = true)
public class Menu implements Serializable {
    private Integer id;
    private Integer parent_id;
    private String name;
    private String perms;
    private String type;
    private String icon;
    private Integer order_num;
    private List<Menu> childrenMenu;
}

package com.springboot.starrysea.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Accessors(chain = true)
public class SysUser implements Serializable {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private String phoneNum;
    private String sex;
    private Date createTime;
    private Integer status;
}

package com.springboot.starrysea.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Accessors(chain = true)
public class Dept implements Serializable {
    private Integer id;
    private String deptName;
    private String upperDept;
}

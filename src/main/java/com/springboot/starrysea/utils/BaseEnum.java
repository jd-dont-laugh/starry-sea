package com.springboot.starrysea.utils;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseEnum {

        /**
         * 获取代码
         * @return
         */
        String getCode();

        /**
         * 获取名称
         * @return
         */
        String getMsg();

}

package com.springboot.starrysea.utils;

import com.springboot.starrysea.entity.Tree;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class MenuTreeUtil {

    public static Map<String,Object> mapArray = new LinkedHashMap<String, Object>();
    public List<Tree> menuCommon;
    public List<Object> list = new ArrayList<Object>();

    public List<Object> menuList(List<Tree> menu){ //controller层调用的方法   ，并将数据以list的形式返回
        this.menuCommon = menu;
        for (Tree x : menu) {
            Map<String,Object> mapArr = new LinkedHashMap<String, Object>();
            if(x.getIsParent().equals("true")&&x.getParentCode().equals("0")){ //判断是否为父极
                mapArr.put("id", x.getId());
                mapArr.put("tree_name", x.getTreeName());
                mapArr.put("parent_code", x.getParentCode());
                mapArr.put("hasChildren", x.getIsParent());
                mapArr.put("tree_img", x.getTreeImg());
                mapArr.put("tree_sort", x.getTreeSort());


                mapArr.put("child", menuChild(x.getId()));  //去子集查找遍历
                list.add(mapArr);
            }
        }
        return list;
    }

    public List<?> menuChild(String id){ //子集查找遍历
        List<Object> lists = new ArrayList<Object>();
        for(Tree a:menuCommon){
            Map<String,Object> childArray = new LinkedHashMap<String, Object>();
            if(a.getParentCode().equals(id) ){
                childArray.put("id", a.getId());
                childArray.put("tree_name", a.getTreeName());
                childArray.put("parent_code", a.getParentCode());

                childArray.put("tree_img", a.getTreeImg());
                childArray.put("tree_sort", a.getTreeSort());
                childArray.put("hasChildren", a.getIsParent());
                childArray.put("child", menuChild(a.getId()));
                lists.add(childArray);
            }
        }
        return lists;
    }

}

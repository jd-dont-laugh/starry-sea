package com.springboot.starrysea.utils.IUtil;

import com.springboot.starrysea.utils.BaseEnum;


/**
 * 状态码枚举实现类
 */
public enum A implements BaseEnum {
    NORMAL("200","正常"),
    DEL("1","删除");

    /**
     * 代码
     */
    private String code;

    /**
     * 名称
     */
    private String msg;

    private A(String code, String msg){
        this.code=code;
        this.msg=msg;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }
}

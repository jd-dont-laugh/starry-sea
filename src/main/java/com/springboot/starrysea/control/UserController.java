package com.springboot.starrysea.control;


import com.springboot.starrysea.service.Impl.IUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class UserController {

    @Autowired
    private IUserService iUserService;
    //登录首页
    @RequestMapping(value = {"/"})
    public String index(){
        return "login";
    }

//    //用户首页
//    @RequestMapping("/index")
//    @RequiresRoles("user")
//    public String indexUser(){
//
//        return "redirect:indexView";
//    }
    @RequestMapping(value = "/logout")
    public String logout() {
        SecurityUtils.getSubject().logout();
        return "redirect:/";
    }

    @RequestMapping("/noPermission")
    public String noPermission(){
        return "noPermission";
    }
//    @PostMapping("/login")
//    public String login(@RequestParam Map<String ,Object> map){
//        LinkedMultiValueMap<Object, Object> paramsMap = new LinkedMultiValueMap<>();
//        paramsMap.set("username",map.get("username"));
//        paramsMap.set("password",map.get("password"));
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(map.))
//    }

    @RequestMapping("/login")
    public String login(String username, String password, Model model){

        //获取权限操作对象，利用这个对象来完成登陆操作
        Subject subject = SecurityUtils.getSubject();

        //进入登录之前，先登出一次，否则shiro会有缓存不能登入
        subject.logout();

        //用户是否认证过（是否登录过）
        if(!subject.isAuthenticated()){
            //创建用户认证时的身份令牌，并设置账号和密码
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
            try {
                //执行登录，会自动调用我们realm对象中的认证方法
                subject.login(usernamePasswordToken);
            }catch (UnknownAccountException e) {
                model.addAttribute("errorMessage", "账号错误！");
                return "login";
            }catch (LockedAccountException e){
                e.printStackTrace();
                model.addAttribute("errorMessage","账号被锁定！");
                return "login";
            }catch (IncorrectCredentialsException e){
                e.printStackTrace();
                model.addAttribute("errorMessage","密码错误！");
                return "login";
            }catch (AuthenticationException e){
                e.printStackTrace();
                model.addAttribute("errorMessage","认证失败");
                return "login";
            }
        }
        return "redirect:index";
    }

    @RequestMapping("/register")
    public String register(String username,String password,Integer roleId){
        return null;
    }

    @RequestMapping("/loginOut")
    public String loginOut(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/";
    }
}

package com.springboot.starrysea.control;

import com.springboot.starrysea.service.Impl.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @Autowired
    private IUserService iUserService;

    @RequestMapping("/distributePermission")
    public String updatePermission(){

        return "/";
    }

    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}

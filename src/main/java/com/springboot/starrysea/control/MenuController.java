package com.springboot.starrysea.control;

import com.springboot.starrysea.entity.Menu;

import com.springboot.starrysea.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author jiajie.tan
 **/
@RestController
@RequestMapping(value = "/menu")
public class MenuController {

    @Autowired
    private SysMenuService sysMenuService;

    @RequestMapping("/getMenuList")
    @ResponseBody
    public List<Menu> menu(){
        List<Menu> menus = sysMenuService.getMenu();
        System.out.println(menus);
        return menus;
    }
}

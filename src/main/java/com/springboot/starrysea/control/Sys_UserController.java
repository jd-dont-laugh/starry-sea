//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.springboot.starrysea.control;

import com.springboot.starrysea.entity.SysUser;
import com.springboot.starrysea.service.Impl.Sys_UserServiceIml;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者:杨俊武
 */
@RestController
@RequestMapping({"/user"})
public class Sys_UserController {
    @Autowired
    private Sys_UserServiceIml sys_userServiceIml;

       /**
     * @param page   页码
     * @param name   用户名
     * @param phone  手机号码
     * @param status 用户状态
     * @return list 用户
     */
    @RequestMapping({"/getAllUser"})
    public List<SysUser> getAllUser(@RequestParam("page") int page,
                                    @RequestParam("name") String name,
                                    @RequestParam("phone") String phone,
//                                    @RequestParam("createTime") Date date,
                                    @RequestParam("status") int status) {
        if (page <= 0) {
            page = 1;
        }
        int limit = (page - 1) * 10;
        List<SysUser> list = sys_userServiceIml.getAllUser(limit, name, phone, status);
        while (list.size() == 0) {
            if(limit==0){
                break;
            }
            limit = (limit / 10 - 1) * 10;
            list = sys_userServiceIml.getAllUser(limit, name, phone, status);

        }
        return list;
    }
}

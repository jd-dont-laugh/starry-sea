package com.springboot.starrysea.service;

import com.springboot.starrysea.entity.SysUser;
import org.springframework.stereotype.Service;

@Service
public interface SysUserService {
    SysUser getUserByName(String username);
    String getRoleById(Integer id);
//    Integer addUser(User user);
    Integer register(SysUser user);

}

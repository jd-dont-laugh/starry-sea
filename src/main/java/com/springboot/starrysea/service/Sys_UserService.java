package com.springboot.starrysea.service;

import com.springboot.starrysea.entity.SysUser;
import java.util.List;
/**
 * 作者:杨俊武
 */
public interface Sys_UserService {
    List<SysUser> getAllUser(int page, String name, String phone, int status);
}

package com.springboot.starrysea.service;


import com.springboot.starrysea.entity.Menu;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SysMenuService {
    List<Menu> getMenu();
}

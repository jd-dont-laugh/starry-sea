package com.springboot.starrysea.service.Impl;

import com.springboot.starrysea.entity.SysUser;
import com.springboot.starrysea.mapper.Sys_UserMapper;
import com.springboot.starrysea.service.Sys_UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 杨俊武
 */
@Service
public class Sys_UserServiceIml implements Sys_UserService {
    @Autowired
    private Sys_UserMapper sys_userMapper;

    public List<SysUser> getAllUser(int page, String name, String phone, int status) {
        return this.sys_userMapper.getAllUser(page, name, phone, status);
    }
}

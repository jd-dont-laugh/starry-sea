package com.springboot.starrysea.service.Impl;

import com.springboot.starrysea.entity.SysUser;
import com.springboot.starrysea.mapper.SysUserMapper;
import com.springboot.starrysea.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IUserService implements SysUserService {

    @Autowired
    private SysUserMapper userMapper;

    @Override
    public SysUser getUserByName(String username) {
        return userMapper.selectUserByName(username);
    }

    @Override
    public String getRoleById(Integer id) {
        return userMapper.getRoleById(id);
    }

//    @Override
//    public Integer addUser(User user) {
//        return null;
//    }

    @Override
    public Integer register(SysUser user) {
        return userMapper.addUser(user);
    }

}

package com.springboot.starrysea.service.Impl;

import com.springboot.starrysea.entity.Menu;

import com.springboot.starrysea.mapper.SysMenuMapper;
import com.springboot.starrysea.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jiajie.tan
 **/
@Service
public class MenuService implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Override
    public List<Menu> getMenu(){
        List<Menu> tree=sysMenuMapper.MenuManagement();
        List<Menu> newMenuList = new ArrayList<>();
        for (int i=0;i<tree.size();i++){
            Menu menu = tree.get(i);
            Integer parent_id = menu.getParent_id();
            if(parent_id==null){
                newMenuList.add(menu);
            }
        }
        for (Menu menu : newMenuList) {
            menu.setChildrenMenu(getChild(menu.getId(), tree));
        }
        return newMenuList;
    }
    private List<Menu> getChild(int id,List<Menu> rootMenu){
        List<Menu> childList = new ArrayList<>();
        for (Menu menu : rootMenu) {
            if(menu.getParent_id()==null){
                continue;
            }
            if(menu.getParent_id()==id){
                childList.add(menu);
            }
        }
        for (Menu menu : childList) {
            menu.setChildrenMenu(getChild(menu.getId(),rootMenu));

        }
        if (childList.size()==0){
            return null;
        }
        return childList;
    }
}

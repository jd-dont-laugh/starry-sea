package com.springboot.starrysea.shiro.realm;


import com.springboot.starrysea.entity.SysUser;
import com.springboot.starrysea.service.Impl.IUserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class Realm extends AuthorizingRealm {

    @Autowired
    private IUserService iUserService;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("------授权了------");

        String obj = (String) principalCollection.getPrimaryPrincipal();//获取用户的账号，根据账号从数据库中获取数据
        Set<String> roles = new HashSet<>();
        Set<String> permissions = new HashSet<>();

        SysUser user = iUserService.getUserByName(obj);
        String role = iUserService.getRoleById(user.getId());
        if ("admin".equals(role)){
            roles.add("admin");
            roles.add("user");
            //为用户添加权限
            permissions.add("admin:");
        }
        if ("user".equals(role)){
            roles.add("user");
        }

        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setRoles(roles);//设置角色信息
        authorizationInfo.setStringPermissions(permissions);//设置权限
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token  = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();//获取页面中传过来的用户账号
        String password = new String(token.getPassword());
        System.out.println(username+" "+password);

        SysUser user = iUserService.getUserByName(username);
        if (user==null){
            throw new UnknownAccountException();
        }
        if (user.getStatus()==1){
            throw new LockedAccountException();
        }

        //设置让当前登录用户中的密码数据进行加密
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashAlgorithmName("MD5");
        credentialsMatcher.setHashIterations(2);
        this.setCredentialsMatcher(credentialsMatcher);
        //对数据库中的密码进行加密
        Object obj  =  new SimpleHash("MD5",user.getPassword(),"",2);

        /**
         * 创建密码认证对象，有shiro自动认证密码
         * 参数1 为数据库里面的账号（或页面账号，均可）
         * 参数2 为数据库中读取出的数据中的密码
         * 参数3 为当前realm的名字
         * 如果密码认证成功则返回一个用户身份对象，否则抛出异常IncorrectCredentialsException
         */
        return new SimpleAuthenticationInfo(username,obj,getName());
    }
}

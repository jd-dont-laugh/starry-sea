package com.springboot.starrysea.mapper;

import com.springboot.starrysea.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author jiajie.tan
 */
@Mapper
public interface SysMenuMapper {
    @Select("select *from menu order by order_num")
    List<Menu> MenuManagement();
}

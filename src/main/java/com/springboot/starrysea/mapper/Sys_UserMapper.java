

package com.springboot.starrysea.mapper;

import com.springboot.starrysea.entity.SysUser;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 作者:杨俊武
 */
@Mapper
public interface Sys_UserMapper {
    List<SysUser> getAllUser(@Param("page") int page, @Param("name") String name, @Param("phone") String phone, @Param("status") int status);
}

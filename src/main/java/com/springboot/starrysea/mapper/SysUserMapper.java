package com.springboot.starrysea.mapper;


import com.springboot.starrysea.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserMapper {
    Integer addUser(SysUser user);
    List<SysUser> selectAllUser();
    SysUser selectUserByName(@Param("N") String username);
    String getRoleById(@Param("X") Integer id);

}
